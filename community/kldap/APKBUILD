# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kldap
pkgver=23.04.1
pkgrel=1
pkgdesc="LDAP access API for KDE"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="
	kcompletion-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	kwidgetsaddons-dev
	openldap-dev
	qtkeychain-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	cyrus-sasl-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kldap-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
79f3550fe1de30b47c9c1ae260a9af737521e273b70f72d3ad471d9fc7e6d7c5dc1876e8cc1c965eeaebcd3dc0ef268a7309ac6e550d539b894cdde092e70009  kldap-23.04.1.tar.xz
"
