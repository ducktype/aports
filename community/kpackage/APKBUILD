# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kpackage
pkgver=5.106.0
pkgrel=0
pkgdesc="Framework that lets applications manage user installable packages of non-binary assets"
arch="all !armhf" # armhf blocked by extra-cmake-modules
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later"
depends_dev="qt5-qtbase-dev karchive-dev ki18n-dev kcoreaddons-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev kdoctools-dev doxygen samurai"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/kpackage-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to requiring installed Plasma, which causes a circular dependency

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
8528d49f3582dc39426e0d6a2299a8050ac1fd569ff60341e75c2aca7af6f332a99d189ff46cf04469d883a13549df25ec538c445d9cd617ff968d13d3cefe6e  kpackage-5.106.0.tar.xz
"
