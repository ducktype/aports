# Contributor: Newbyte <newbyte@disroot.org>
# Maintainer: Newbyte <newbyte@disroot.org>
pkgname=py3-pydantic
pkgver=1.10.8
pkgrel=0
pkgdesc="Data parsing and validation using Python type hints"
url="https://github.com/samuelcolvin/pydantic"
arch="noarch"
license="MIT"
depends="python3 py3-typing-extensions"
makedepends="py3-setuptools"
checkdepends="py3-hypothesis py3-pytest py3-pytest-mock"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/samuelcolvin/pydantic/archive/refs/tags/v$pkgver.tar.gz
	deprecation-warning.patch"
builddir="$srcdir/pydantic-$pkgver"

build() {
	python3 setup.py build
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages test-env
	test-env/bin/python3 setup.py install
	test-env/bin/python3 -m pytest -W ignore::DeprecationWarning
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
3ac41cdf0eb70fb71298131a043966b85387bc953ef2f463ece80728b46251d5d5f66c3f030afc3cdf4527918ae410fcd733a774cbe0c3b7ba9fc806a76378e4  py3-pydantic-1.10.8.tar.gz
20fe12362cbe6a7d9f393eea71f8aa968e4a09cd6e4594505a95f889b5ec27f06c1214ef8aa841f3aa5b57f6c9bb41fbe382ef7512724e5e0947e2e916f495c9  deprecation-warning.patch
"
