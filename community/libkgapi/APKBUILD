# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=libkgapi
pkgver=23.04.1
pkgrel=0
pkgdesc="LibKGAPI is a KDE-based library for accessing various Google services via their public API"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://community.kde.org/KDE_PIM"
license="LGPL-2.1-only OR LGPL-3.0-only"
depends_dev="
	cyrus-sasl-dev
	kcalendarcore-dev
	kcontacts-dev
	kio-dev
	kwallet-dev
	kwindowsystem-dev
	qt5-qtbase-dev
	qt5-qtwebengine-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
source="https://download.kde.org/stable/release-service/$pkgver/src/libkgapi-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# calendar-freebusyqueryjobtest, contacts-contactfetchjobtest,
	# contacts-contactcreatejobtest and contacts-contactmodifyjobtest are broken
	# tasks-taskfetchjobtest fails on the builders
	# contacts-contactfetchjobtest: https://gitlab.alpinelinux.org/alpine/aports/-/issues/11787
	# calendar-event-*: UTC+02 != UTC+02:00
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "(tasks-taskfetchjob|calendar-freebusyqueryjob|contacts-contact(fetchjob|createjob|modifyjob)|calendar-event(createjob|modifyjob|fetchjob))test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
512e3e85a8e638eb013de1357ffb52394f8c38c33f0e9f0c86e27d5f4f853f3ec681c40e652e532cce1a0c946e8d81557577e9dbe079e180cb2f34c2d2a3118e  libkgapi-23.04.1.tar.xz
"
