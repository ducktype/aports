# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=calindori
pkgver=23.04.1
pkgrel=0
pkgdesc="Calendar for Plasma Mobile"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://invent.kde.org/plasma-mobile/calindori"
license="GPL-3.0-or-later AND LGPL-3.0-or-later AND BSD-2-Clause AND CC-BY-SA-4.0 AND CC0-1.0"
depends="
	kirigami2
	qt5-qtquickcontrols
	qt5-qtquickcontrols2
	"
makedepends="
	extra-cmake-modules
	kcalendarcore-dev
	kconfig-dev
	ki18n-dev
	kirigami2-dev
	kpeople-dev
	plasma-framework-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	qt5-qttools-dev
	samurai
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/calindori-$pkgver.tar.xz"
subpackages="$pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
90aa9de3f137d2c2100de55c0f8122a3476a35d5a069301a0b79dfe79ad179c973271bd3e589bf10cb05592805519f85cf9117b4448a503aa39606a20b7be755  calindori-23.04.1.tar.xz
"
