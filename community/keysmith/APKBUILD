# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=keysmith
pkgver=23.04.1
pkgrel=0
pkgdesc="OTP client for Plasma Mobile and Desktop"
url="https://invent.kde.org/kde/keysmith"
arch="all !armhf"
license="GPL-3.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	ki18n-dev
	kirigami2-dev
	libsodium-dev
	qt5-qtbase-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
source="https://download.kde.org/stable/release-service/$pkgver/src/keysmith-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
9e24ceab16f369a3b03c4c52307f4484e9e4896022725a6bc61e66cf736ea0936a3dca56e0ecd652beb9a151b17b8187b89063918b179994cb1fb8f27eb79532  keysmith-23.04.1.tar.xz
"
