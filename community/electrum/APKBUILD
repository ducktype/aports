# Maintainer: Michał Adamski <michal@ert.pl>
pkgname=electrum
pkgver=4.4.3
pkgrel=0
pkgdesc="Lightweight Bitcoin Wallet"
url="https://electrum.org/"
arch="noarch"
license="MIT"
depends="
	libsecp256k1
	py3-aiohttp
	py3-aiohttp-socks
	py3-aiorpcx
	py3-attrs
	py3-bitstring
	py3-certifi
	py3-dnspython
	py3-ecdsa
	py3-protobuf
	py3-pycryptodomex
	py3-qrcode
	"
makedepends="
	py3-gpep517
	py3-setuptools
	py3-wheel
	"
checkdepends="py3-pytest-xdist py3-cryptography py3-pyaes"
subpackages="$pkgname-pyc"
source="
	electrum-$pkgver.tar.gz::https://github.com/spesmilo/electrum/archive/refs/tags/$pkgver.tar.gz
	0001-apk-add-instead-of-apt-get-install.patch
"

build() {
	gpep517 build-wheel \
		--wheel-dir dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages testenv
	testenv/bin/python3 -m installer dist/*.whl
	testenv/bin/python3 -m pytest -n auto electrum/tests
}

package() {
	python3 -m installer -d "$pkgdir" \
		dist/*.whl
}

sha512sums="
077742c404cce57fbf330b28a36c277dc22c10027c8f412ea192a7f7b917b37b22bbb85dc6cdc654daaacc28f98659dac406879a183448b20b0377a86697f486  electrum-4.4.3.tar.gz
1ac73ac652d402e0ca9b59c021dc54bf728ffab1af12997d859884cdd1b092b745640e31684f2bd01a345af9c320ebc29a90e83ebf799c3eec13b52e6e119c5a  0001-apk-add-instead-of-apt-get-install.patch
"
