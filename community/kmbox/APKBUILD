# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kmbox
pkgver=23.04.1
pkgrel=0
pkgdesc="Library for accessing mail storages in MBox format"
# armhf blocked by extra-cmake-modules
# s390x blocked by kmime
arch="all !armhf !s390x"
url="https://kontact.kde.org/"
license="LGPL-2.0-or-later"
depends_dev="kmime-dev"
makedepends="$depends_dev extra-cmake-modules samurai"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmbox-$pkgver.tar.xz"
subpackages="$pkgname-dev"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --build build --target install
}

sha512sums="
af3af96ea3251f9a53fd6c4013d44542774e71bbb6a573b61dac6eb4ef724db918efe9349e60d690fab897c3f1bca0771f448d92a0db6a82d793f4e82799960e  kmbox-23.04.1.tar.xz
"
