# Contributor: Rudolf Polzer <divVerent@gmail.com>
# Maintainer: Rudolf Polzer <divVerent@gmail.com>
pkgname=aaaaxy
pkgver=1.4.2
pkgrel=0
pkgdesc="A nonlinear puzzle platformer taking place in impossible spaces"
url="https://divVerent.github.io/aaaaxy/"
arch="all !s390x !armhf !armv7 !riscv64"
license="Apache-2.0"
makedepends="
	advancecomp
	alsa-lib-dev
	go
	libx11-dev
	libxcursor-dev
	libxinerama-dev
	libxi-dev
	libxrandr-dev
	mesa-dev
	zip
"
checkdepends="xvfb-run mesa-dri-gallium"
source="
	https://github.com/divVerent/aaaaxy/archive/v$pkgver/aaaaxy-$pkgver.tar.gz
	https://github.com/divVerent/aaaaxy/releases/download/v$pkgver/sdl-gamecontrollerdb-for-aaaaxy-v$pkgver.zip
"
options="net"  # Needed for go mod download.

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

prepare() {
	default_prepare

	mv "$srcdir"/third_party/SDL_GameControllerDB/assets/input/* \
		third_party/SDL_GameControllerDB/assets/input/
	go mod download
}

build() {
	export AAAAXY_BUILD_USE_VERSION_FILE=true
	make BUILDTYPE=release
}

check() {
	xvfb-run sh scripts/regression-test-demo.sh aaaaxy \
		"on track for Any%, All Paths, No Teleports and No Coil" \
		./aaaaxy assets/demos/benchmark.dem
}

package() {
	install -Dm644 io.github.divverent.aaaaxy.metainfo.xml \
		"$pkgdir"/usr/share/metainfo/io.github.divverent.aaaaxy.metainfo.xml
	install -Dm644 aaaaxy.png \
		"$pkgdir"/usr/share/icons/hicolor/128x128/apps/aaaaxy.png
	install -Dm644 aaaaxy.desktop \
		"$pkgdir"/usr/share/applications/aaaaxy.desktop
	install -Dm755 aaaaxy \
		"$pkgdir"/usr/bin/aaaaxy
}

sha512sums="
207026ce57404d3f5398314d32f22ac90731b6187897a7fe817fec4b6b490305a05e827e076e6b00370539c609ae88a8cb2bfd68a635eb0733d4d2362f67b9ff  aaaaxy-1.4.2.tar.gz
a4cd865256bc7e31942daf499b85634878201a5e7124ce5c8bd9ea1a3e3891808dcf089c9ede421fda9aefc937ed8e2154a829db3064516dd528d7f842150ba9  sdl-gamecontrollerdb-for-aaaaxy-v1.4.2.zip
"
