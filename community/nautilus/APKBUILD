# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: team/gnome <ablocorrea@hotmail.com>
pkgname=nautilus
pkgver=44.2
pkgrel=0
pkgdesc="GNOME file manager"
url="https://wiki.gnome.org/Apps/Nautilus"
# s390x blocked by mozjs91 -> tracker-miners
arch="all !s390x"
license="GPL-2.0-or-later"
depends="
	gsettings-desktop-schemas
	tracker-miners
	"
depends_dev="
	gnome-autoar-dev
	gnome-desktop-dev
	"
makedepends="
	$depends_dev
	desktop-file-utils
	docbook-xml
	docbook-xsl
	gexiv2-dev
	gst-plugins-base-dev
	itstool
	jpeg-dev
	libadwaita-dev
	libcloudproviders-dev
	libexif-dev
	libportal-dev
	librsvg-dev
	libseccomp-dev
	libxml2-dev
	libxml2-utils
	libxslt
	meson
	tiff-dev
	tracker-dev
	tracker-testutils
	"
checkdepends="dbus appstream-glib"
subpackages="$pkgname-dev $pkgname-lang"
source="https://download.gnome.org/sources/nautilus/${pkgver%.*}/nautilus-$pkgver.tar.xz
	meson-fix-mixed-list.patch
	"

if [ "$CARCH" = "armhf" ]; then
	options="!check" # one test fails on armhf
fi

# secfixes:
#   3.32.1-r0:
#     - CVE-2019-11461

build() {
	abuild-meson \
		-Db_lto=true \
		-Dtests=headless \
		. output
	meson compile -C output
}

check() {
	XDG_RUNTIME_DIR="$(mktemp -d -p "$builddir")" \
	dbus-run-session -- \
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

sha512sums="
1bec622d204e9ae01dfcab37e3263927c723ab1e143729190777c45cae6f03d6f601fd30bc1a75dbba63b2b69f212e888bacf2eebad118f8b4c5e09134aac4b6  nautilus-44.2.tar.xz
d08c6833343b8ed89515d12063938e7236cb37d15b7115de50a43bd9a049a09a5a2fc907f52675ba85abecef9bae82c7848249872908eeb39c5c49a744cbf9d1  meson-fix-mixed-list.patch
"
