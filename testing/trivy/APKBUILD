# Contributor: TBK <alpine@jjtc.eu>
# Maintainer: TBK <alpine@jjtc.eu>
pkgname=trivy
pkgver=0.42.0
pkgrel=0
pkgdesc="Simple and comprehensive vulnerability scanner for containers"
url="https://github.com/aquasecurity/trivy"
arch="all"
# s390x: tests SIGSEGV: https://github.com/aquasecurity/trivy/issues/430
arch="$arch !s390x"
license="Apache-2.0"
makedepends="btrfs-progs-dev go linux-headers lvm2-dev"
source="https://github.com/aquasecurity/trivy/archive/v$pkgver/trivy-$pkgver.tar.gz"
options="net !check" # needs tinygo to turn go into wasm for tests

export GOCACHE="${GOCACHE:-"$srcdir/go-cache"}"
export GOTMPDIR="${GOTMPDIR:-"$srcdir"}"
export GOMODCACHE="${GOMODCACHE:-"$srcdir/go"}"

build() {
	go build -o trivy -ldflags "-X main.version=$pkgver" cmd/trivy/main.go
}

check() {
	go test ./...
}

package() {
	install -Dm755 trivy -t "$pkgdir"/usr/bin/
}

sha512sums="
81b28dbaa0f2a56ef9d04543c01a8efa35295fdad4fa2c577b86af48c2a855e6c1ea61638d35a535b28e5092276bf6164ca35b084b3f5c2dd4081792828e5046  trivy-0.42.0.tar.gz
"
